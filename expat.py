#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.basemap import Basemap
from netCDF4 import Dataset


# Thanks to http://www.hydro.washington.edu/~jhamman/hydro-logic/blog/2013/10/12/plot-netcdf-data/
# for useful tips on getting things started.

leap_years = (1980, 1984, 1988, 1992, 1996, 2000, 2004, 2008, 2012, 2016)


def load_part(fname, beg, end):
    fh = Dataset(fname, mode='r')
    lats = fh.variables['latitude'][:]
    lons = fh.variables['longitude'][:]
    times = fh.variables['time'][beg : end]
    t2m = fh.variables['t2m'][beg : end]
    fh.close()
    print("Loaded from {} to {}".format(
        np.datetime64('1900', 'h') + times[0],
        np.datetime64('1900', 'h') + times[-1]))
    return lats, lons, times, t2m


def year_range(year):
    num_leap_years_before_year = np.searchsorted(np.array(leap_years), year)
    beg = (year - 1979) * 4 * 365 + (num_leap_years_before_year * 4)
    days_in_year = 366 if year in leap_years else 365
    end = beg + days_in_year * 4
    return beg, end + 1


def minmax(data):
    return data.min(), data.max()


def get_extremes():
    for year in range(1979, 2016):
        beg, end = year_range(year)
        lats, lons, times, t2m = load_part("t2m.nc", beg, end)
        ymin, ymax = minmax(t2m)
        allmin = min(allmin, ymin)
        allmax = max(allmax, ymax)
        print("LOG year {} min {} max {}".format(year, ymin, ymax))
    return allmin, allmax


def do_day(day, figargs, lats, lons, times, t2m, lat, lon, lat_0, lon_0,
           allmin, allmax):
    fig = plt.figure(**figargs)
    parts = list(range(4))
    for part in parts:
        idx = day * len(parts) + part
        ax = fig.add_subplot(2, 2, part + 1)
        ax.set_title(str(np.datetime64('1900', 'h') + times[idx]))
        m = Basemap(width=5000000, height=3500000, resolution='l',
                    projection='stere', lat_0=lat_0, lon_0=lon_0)
        K2C = lambda K: K - 272.150
        data = K2C(t2m[idx])
        vmin = K2C(allmin)
        vmax = K2C(allmax)
        xi, yi = m(lon, lat)
        cs = m.pcolor(xi, yi, data, vmin=vmin, vmax=vmax)
        m.drawcoastlines()
        cbar = m.colorbar(cs, location='left' if part % 2 == 0 else 'right',
                          pad='12%')
        cbar.set_label('°C')

def do_year(year, allmin, allmax):
    figargs = {'dpi': 80, 'figsize': (24.74, 12.98)}
    beg, end = year_range(year)
    print("Loading from {} to {}".format(beg, end))
    lats, lons, times, t2m = load_part("t2m.nc", beg, end)
    lon, lat = np.meshgrid(lons, lats)
    lat_0, lon_0 = lats.mean(), lons.mean()
    for day in range(0, 366 if year in leap_years else 365):
        do_day(day, figargs, lats, lons, times, t2m, lat, lon, lat_0, lon_0,
               allmin, allmax)
        plt.savefig('expat-{:04d}-{:03d}.png'.format(year, day),
                    bbox_inches='tight', pad_inches=0.1)
        plt.close('all')
        print("[{}] done year {} day {} ({})".format(
            datetime.now(), year, day + 1,
            np.datetime64('{}'.format(year), 'D') + day))

# in case you don't want to run the code for some minutes
allmin, allmax = (220.251922607, 325.055419922)

# in case you want
#allmin, allmax = get_extremes()

if __name__ == '__main__':
    import sys
    do_year(int(sys.argv[1]), allmin, allmax)
